<?php

return
[
    [
        'title' => ['en' =>  "AlFatihah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlBaq'ar'ah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "Al'Imran"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AnNisa'"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlMa'idah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlAn'am"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlA'raf"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlAnfal"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AtTaubah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "Yunus"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Hud"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Yusuf"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "'Ar'Ra'd"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "Ibrahim"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlHijr"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNahl"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlIsra'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlKahf"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "M'ar'yam"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "TaHa"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlAnbiya'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlHajj"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlMu'minun"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNur"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlFurqan"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AshShu''ar'a'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNaml"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlQasas"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlAnkabut"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "'Ar'Rum"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Luqman"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AsSajdah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlAhzab"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "Saba'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Fatir"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "YaSin"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AsSaffat"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Sad"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AzZum'ar'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Ghafir"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Fussilat"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AshShura"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AzZukhruf"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AdDukhan"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlJathiyah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlAhqaf"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Muhammad"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlFath"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlHujurat"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "Qaf"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AdhDh'ar'iyat"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AtTur"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNajm"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Qam'ar'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "'Ar'Rahman"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlWaqi'ah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlHadid"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlMujadilah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlHashr"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlMumtahanah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AsSaff"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlJumu'ah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlMunafiqun"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AtTaghabun"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AtTalaq"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AtTahrim"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlMulk"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlQalam"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlHaqqah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlMa''ar'ij"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Nuh"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlJinn"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlMuzzammil"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlMuddaththir"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlQiyamah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlInsan"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlMursalat"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNaba'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNazi'at"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Abasa"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AtTakwir"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlInfit'ar'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlMutaffifin"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlInshiqaq"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlBuruj"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AtT'ar'iq"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlA'la"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlGhashiyah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlFajr"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlBalad"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AshShams"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlLail"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AdDuha"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AshSh'ar'h"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AtTin"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlAlaq"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlQadr"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlBayyinah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AzZalzalah"],
        'place' => ['en' =>  "Medina"]
    ],
    [
        'title' => ['en' =>  "AlAadiyat"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlQ'ar'i'ah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AtTakathur"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlAsr"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlHumazah"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlFil"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "Quraish"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlMa'un"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlKauth'ar'"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlKafirun"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNasr"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlMasad"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlIkhlas"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AlFalaq"],
        'place' => ['en' =>  "Mecca"]
    ],
    [
        'title' => ['en' =>  "AnNas"],
        'place' => ['en' =>  "Mecca"]
    ]
];
