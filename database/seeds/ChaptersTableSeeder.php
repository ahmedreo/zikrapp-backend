<?php

use Faker\Factory as Facker ;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ChaptersTableSeeder extends seeder
{
	public function run()
	{
		$facker = Facker::create();

		App\Chapter::insert(
			[
				[
				// 'title' => ['en' =>  "AlFatihah", 'ar' =>  "الفاتحة"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "7"
				],
				[
				// 'title' => ['en' =>  "AlBaq'ar'ah", 'ar' =>  "البقرة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "286"
				],
				[
				// 'title' => ['en' =>  "Al'Imran", 'ar' =>  "آل عمران"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "200"
				],
				[
				// 'title' => ['en' =>  "AnNisa'", 'ar' =>  "النساء"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "176"
				],
				[
				// 'title' => ['en' =>  "AlMa'idah", 'ar' =>  "المائدة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "120"
				],
				[
				// 'title' => ['en' =>  "AlAn'am", 'ar' =>  "الأنعام"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "165"
				],
				[
				// 'title' => ['en' =>  "AlA'raf", 'ar' =>  "الأعراف"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "206"
				],
				[
				// 'title' => ['en' =>  "AlAnfal", 'ar' =>  "الأنفال"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "75"
				],
				[
				// 'title' => ['en' =>  "AtTaubah", 'ar' =>  "التوبة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "129"
				],
				[
				// 'title' => ['en' =>  "Yunus", 'ar' =>  "يونس"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "109"
				],
				[
				// 'title' => ['en' =>  "Hud", 'ar' =>  "هود"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "123"
				],
				[
				// 'title' => ['en' =>  "Yusuf", 'ar' =>  "يوسف"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "111"
				],
				[
				// 'title' => ['en' =>  "'Ar'Ra'd", 'ar' =>  "الرعد"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "43"
				],
				[
				// 'title' => ['en' =>  "Ibrahim", 'ar' =>  "إبراهيم"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "52"
				],
				[
				// 'title' => ['en' =>  "AlHijr", 'ar' =>  "الحجر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "99"
				],
				[
				// 'title' => ['en' =>  "AnNahl", 'ar' =>  "النحل"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "128"
				],
				[
				// 'title' => ['en' =>  "AlIsra'", 'ar' =>  "الإسراء"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "111"
				],
				[
				// 'title' => ['en' =>  "AlKahf", 'ar' =>  "الكهف"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "110"
				],
				[
				// 'title' => ['en' =>  "M'ar'yam", 'ar' =>  "مريم"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "98"
				],
				[
				// 'title' => ['en' =>  "TaHa", 'ar' =>  "طه"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "135"
				],
				[
				// 'title' => ['en' =>  "AlAnbiya'", 'ar' =>  "الأنبياء"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "112"
				],
				[
				// 'title' => ['en' =>  "AlHajj", 'ar' =>  "الحج"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "78"
				],
				[
				// 'title' => ['en' =>  "AlMu'minun", 'ar' =>  "المؤمنون"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "118"
				],
				[
				// 'title' => ['en' =>  "AnNur", 'ar' =>  "النور"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "64"
				],
				[
				// 'title' => ['en' =>  "AlFurqan", 'ar' =>  "الفرقان"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "77"
				],
				[
				// 'title' => ['en' =>  "AshShu''ar'a'", 'ar' =>  "الشعراء"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "227"
				],
				[
				// 'title' => ['en' =>  "AnNaml", 'ar' =>  "النمل"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "93"
				],
				[
				// 'title' => ['en' =>  "AlQasas", 'ar' =>  "القصص"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "88"
				],
				[
				// 'title' => ['en' =>  "AlAnkabut", 'ar' =>  "العنكبوت"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "69"
				],
				[
				// 'title' => ['en' =>  "'Ar'Rum", 'ar' =>  "الروم"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "60"
				],
				[
				// 'title' => ['en' =>  "Luqman", 'ar' =>  "لقمان"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "34"
				],
				[
				// 'title' => ['en' =>  "AsSajdah", 'ar' =>  "السجدة"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "30"
				],
				[
				// 'title' => ['en' =>  "AlAhzab", 'ar' =>  "الأحزاب"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "73"
				],
				[
				// 'title' => ['en' =>  "Saba'", 'ar' =>  "سبأ"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "54"
				],
				[
				// 'title' => ['en' =>  "Fatir", 'ar' =>  "فاطر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "45"
				],
				[
				// 'title' => ['en' =>  "YaSin", 'ar' =>  "يس"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "83"
				],
				[
				// 'title' => ['en' =>  "AsSaffat", 'ar' =>  "الصافات"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "182"
				],
				[
				// 'title' => ['en' =>  "Sad", 'ar' =>  "ص"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "88"
				],
				[
				// 'title' => ['en' =>  "AzZum'ar'", 'ar' =>  "الزمر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "75"
				],
				[
				// 'title' => ['en' =>  "Ghafir", 'ar' =>  "غافر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "85"
				],
				[
				// 'title' => ['en' =>  "Fussilat", 'ar' =>  "فصلت"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "54"
				],
				[
				// 'title' => ['en' =>  "AshShura", 'ar' =>  "الشورى"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "53"
				],
				[
				// 'title' => ['en' =>  "AzZukhruf", 'ar' =>  "الزخرف"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "89"
				],
				[
				// 'title' => ['en' =>  "AdDukhan", 'ar' =>  "الدخان"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "59"
				],
				[
				// 'title' => ['en' =>  "AlJathiyah", 'ar' =>  "الجاثية"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "37"
				],
				[
				// 'title' => ['en' =>  "AlAhqaf", 'ar' =>  "الأحقاف"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "35"
				],
				[
				// 'title' => ['en' =>  "Muhammad", 'ar' =>  "محمد"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "38"
				],
				[
				// 'title' => ['en' =>  "AlFath", 'ar' =>  "الفتح"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "29"
				],
				[
				// 'title' => ['en' =>  "AlHujurat", 'ar' =>  "الحجرات"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "18"
				],
				[
				// 'title' => ['en' =>  "Qaf", 'ar' =>  "ق"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "45"
				],
				[
				// 'title' => ['en' =>  "AdhDh'ar'iyat", 'ar' =>  "الذاريات"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "60"
				],
				[
				// 'title' => ['en' =>  "AtTur", 'ar' =>  "الطور"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "49"
				],
				[
				// 'title' => ['en' =>  "AnNajm", 'ar' =>  "النجم"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "62"
				],
				[
				// 'title' => ['en' =>  "Qam'ar'", 'ar' =>  "القمر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "55"
				],
				[
				// 'title' => ['en' =>  "'Ar'Rahman", 'ar' =>  "الرحمن"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "78"
				],
				[
				// 'title' => ['en' =>  "AlWaqi'ah", 'ar' =>  "الواقعة"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "96"
				],
				[
				// 'title' => ['en' =>  "AlHadid", 'ar' =>  "الحديد"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "29"
				],
				[
				// 'title' => ['en' =>  "AlMujadilah", 'ar' =>  "المجادلة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "22"
				],
				[
				// 'title' => ['en' =>  "AlHashr", 'ar' =>  "الحشر"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "24"
				],
				[
				// 'title' => ['en' =>  "AlMumtahanah", 'ar' =>  "الممتحنة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "13"
				],
				[
				// 'title' => ['en' =>  "AsSaff", 'ar' =>  "الصف"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "14"
				],
				[
				// 'title' => ['en' =>  "AlJumu'ah", 'ar' =>  "الجمعة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "11"
				],
				[
				// 'title' => ['en' =>  "AlMunafiqun", 'ar' =>  "المنافقون"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "11"
				],
				[
				// 'title' => ['en' =>  "AtTaghabun", 'ar' =>  "التغابن"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "18"
				],
				[
				// 'title' => ['en' =>  "AtTalaq", 'ar' =>  "الطلاق"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "12"
				],
				[
				// 'title' => ['en' =>  "AtTahrim", 'ar' =>  "التحريم"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "12"
				],
				[
				// 'title' => ['en' =>  "AlMulk", 'ar' =>  "الملك"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "30"
				],
				[
				// 'title' => ['en' =>  "AlQalam", 'ar' =>  "القلم"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "52"
				],
				[
				// 'title' => ['en' =>  "AlHaqqah", 'ar' =>  "الحاقة"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "52"
				],
				[
				// 'title' => ['en' =>  "AlMa''ar'ij", 'ar' =>  "المعارج"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "44"
				],
				[
				// 'title' => ['en' =>  "Nuh", 'ar' =>  "نوح"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "28"
				],
				[
				// 'title' => ['en' =>  "AlJinn", 'ar' =>  "الجن"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "28"
				],
				[
				// 'title' => ['en' =>  "AlMuzzammil", 'ar' =>  "المزمل"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "20"
				],
				[
				// 'title' => ['en' =>  "AlMuddaththir", 'ar' =>  "المدثر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "56"
				],
				[
				// 'title' => ['en' =>  "AlQiyamah", 'ar' =>  "القيامة"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "40"
				],
				[
				// 'title' => ['en' =>  "AlInsan", 'ar' =>  "الإنسان"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "31"
				],
				[
				// 'title' => ['en' =>  "AlMursalat", 'ar' =>  "المرسلات"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "50"
				],
				[
				// 'title' => ['en' =>  "AnNaba'", 'ar' =>  "النبأ"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "40"
				],
				[
				// 'title' => ['en' =>  "AnNazi'at", 'ar' =>  "النازعات"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "46"
				],
				[
				// 'title' => ['en' =>  "Abasa", 'ar' =>  "عبس"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "42"
				],
				[
				// 'title' => ['en' =>  "AtTakwir", 'ar' =>  "التكوير"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "29"
				],
				[
				// 'title' => ['en' =>  "AlInfit'ar'", 'ar' =>  "الانفطار"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "19"
				],
				[
				// 'title' => ['en' =>  "AlMutaffifin", 'ar' =>  "المطففين"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "36"
				],
				[
				// 'title' => ['en' =>  "AlInshiqaq", 'ar' =>  "الانشقاق"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "25"
				],
				[
				// 'title' => ['en' =>  "AlBuruj", 'ar' =>  "البروج"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "22"
				],
				[
				// 'title' => ['en' =>  "AtT'ar'iq", 'ar' =>  "الطارق"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "17"
				],
				[
				// 'title' => ['en' =>  "AlA'la", 'ar' =>  "الأعلى"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "19"
				],
				[
				// 'title' => ['en' =>  "AlGhashiyah", 'ar' =>  "الغاشية"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "26"
				],
				[
				// 'title' => ['en' =>  "AlFajr", 'ar' =>  "الفجر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "30"
				],
				[
				// 'title' => ['en' =>  "AlBalad", 'ar' =>  "البلد"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "20"
				],
				[
				// 'title' => ['en' =>  "AshShams", 'ar' =>  "الشمس"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "15"
				],
				[
				// 'title' => ['en' =>  "AlLail", 'ar' =>  "الليل"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "21"
				],
				[
				// 'title' => ['en' =>  "AdDuha", 'ar' =>  "الضحى"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "11"
				],
				[
				// 'title' => ['en' =>  "AshSh'ar'h", 'ar' =>  "الشرح"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "8"
				],
				[
				// 'title' => ['en' =>  "AtTin", 'ar' =>  "التين"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "8"
				],
				[
				// 'title' => ['en' =>  "AlAlaq", 'ar' =>  "العلق"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "19"
				],
				[
				// 'title' => ['en' =>  "AlQadr", 'ar' =>  "القدر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "5"
				],
				[
				// 'title' => ['en' =>  "AlBayyinah", 'ar' =>  "البينة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "8"
				],
				[
				// 'title' => ['en' =>  "AzZalzalah", 'ar' =>  "الزلزلة"],
				// 'place' =>['en' =>  "Medina", 'ar' =>  "مدنية"],
				'verses' => "8"
				],
				[
				// 'title' => ['en' =>  "AlAadiyat", 'ar' =>  "العاديات"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "11"
				],
				[
				// 'title' => ['en' =>  "AlQ'ar'i'ah", 'ar' =>  "القارعة"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "11"
				],
				[
				// 'title' => ['en' =>  "AtTakathur", 'ar' =>  "التكاثر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "8"
				],
				[
				// 'title' => ['en' =>  "AlAsr", 'ar' =>  "العصر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "3"
				],
				[
				// 'title' => ['en' =>  "AlHumazah", 'ar' =>  "الهمزة"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "9"
				],
				[
				// 'title' => ['en' =>  "AlFil", 'ar' =>  "الفيل"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "5"
				],
				[
				// 'title' => ['en' =>  "Quraish", 'ar' =>  "قريش"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "4"
				],
				[
				// 'title' => ['en' =>  "AlMa'un", 'ar' =>  "الماعون"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "7"
				],
				[
				// 'title' => ['en' =>  "AlKauth'ar'", 'ar' =>  "الكوثر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "3"
				],
				[
				// 'title' => ['en' =>  "AlKafirun", 'ar' =>  "الكافرون"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "6"
				],
				[
				// 'title' => ['en' =>  "AnNasr", 'ar' =>  "النصر"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "3"
				],
				[
				// 'title' => ['en' =>  "AlMasad", 'ar' =>  "المسد"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "5"
				],
				[
				// 'title' => ['en' =>  "AlIkhlas", 'ar' =>  "الإخلاص"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "4"
				],
				[
				// 'title' => ['en' =>  "AlFalaq", 'ar' =>  "الفلق"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "5"
				],
				[
				// 'title' => ['en' =>  "AnNas", 'ar' =>  "الناس"],
				// 'place' =>['en' =>  "Mecca", 'ar' =>  "مكية"],
				'verses' => "6"
				]
			]

		);
		
		App\Chapter::all()->each(function ($u , $k)
		{
			$facker = Facker::create();

			$u->index = $k+1;

			$u->save();
		});	
	}
}