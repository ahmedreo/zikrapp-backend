<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		App\Reciter::truncate();

		$this->call(RecitersTableSeeder::class);

		App\Chapter::truncate();
		
		$this->call(ChaptersTableSeeder::class);

		App\Riwaya::truncate();

		$this->call(RiwayasTableSeeder::class);

		App\Track::truncate();

		$this->call(TracksTableSeeder::class);

		App\Playlist::truncate();

		$this->call(PlaylistsTableSeeder::class);

		App\User::truncate();

		$this->call(UsersTableSeeder::class);
	}

}
