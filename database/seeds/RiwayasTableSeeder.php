<?php

use Faker\Factory as Facker ;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RiwayasTableSeeder extends seeder
{
	public function run()
	{
		$facker = Facker::create();

		foreach (range (1, 10) as $index) {

			App\Riwaya::create([

				'title' => $facker->sentence($nbWords = 1, $variableNbWords = true),

			]);
		}
	}
}