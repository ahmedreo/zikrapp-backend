<?php

use Faker\Factory as Facker ;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RecitersTableSeeder extends seeder
{
	public function run()
	{
		$facker = Facker::create();

		foreach (range (1, 40) as $index) {

			App\Reciter::create([

				'name' => $facker->name,
				'country' => $facker->country,
				'image' => $facker->imageUrl($width = 640, $height = 480 ,'people')

			]);
		}
	}
}