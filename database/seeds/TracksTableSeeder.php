<?php

use Faker\Factory as Facker ;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TracksTableSeeder extends seeder
{
	public function run()
	{

		$facker = Facker::create();
			
		foreach (range (1, 60) as $index)
		{
			App\Track::create([
				'title' => $facker->sentence($nbWords = 1, $variableNbWords = true),
				'url' => $facker->numerify('http://server11.mp3quran.net/shatri/0##.mp3'),
			]);
		}

		App\Track::all()->each(function ($u)
		{
			$facker = Facker::create();

			$facker_reciter_id = $facker->randomElement(App\Reciter::get()->lists('id')->toArray());
			$facker_chapter_id = $facker->randomElement(App\Chapter::get()->lists('id')->toArray());
			$facker_riwaya_id =  $facker->randomElement(App\Riwaya::get()->lists('id')->toArray());

			$u->push('foriegnKeys', [
				'reciter_body' => App\Reciter::find($facker_reciter_id)->toArray(),
				'chapter_body' => App\Chapter::find($facker_chapter_id)->toArray(),
				'riwaya_body' => App\Riwaya::find($facker_riwaya_id)->toArray()
			]);
	
		});
	}
}