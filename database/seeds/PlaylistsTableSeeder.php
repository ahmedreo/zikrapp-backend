<?php

use Faker\Factory as Facker ;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PlaylistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facker = Facker::create();
			
		foreach (range (1, 30) as $index)
		{
			App\Playlist::create([
				'title' => $facker->sentence($nbWords = 2, $variableNbWords = true),
			]);
		}

		App\Playlist::all()->each(function ($u)
		{
			$facker = Facker::create();


			foreach (range (1, 3) as $index)
			{

			$facker_id = $facker->unique()->randomElement(App\Track::get()->lists('id')->toArray());

			$u->push('foriegnKeys', [
				'track_body' => App\Track::find($facker_id)->toArray(),
				'sort_id' => $facker->numberBetween($min = 1, $max = 50)
			]);
				
			}
	
		});
    }
}
