## Welcome to Zikrapp Api Documintation

This Documentaion helps you dealing with all API you need to achieve your Front-End ِِAِpplication
and get your resources easily . 

## Official Repository

Repository for the Api can be found on the [zikrapp-backend](https://bitbucket.org/ahmedreo/zikrapp-backend).


## Security Vulnerabilities

If you discover a security vulnerability within API, please send an e-mail to Ahmed Yasser at ahmedreo4@gmail.com. All security vulnerabilities will be promptly addressed.

<!-- ## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT). -->
