<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => 'Lang'], function () {
	Route::resource('user', 'UserController');
	Route::resource('reciter', 'ReciterController');
	Route::resource('playlist', 'PlaylistController');
	Route::resource('track', 'TrackController');
	Route::resource('chapter', 'ChapterController');
	Route::resource('riwaya', 'RiwayaController');    
});


Route::post('login' , 'Auth\AuthController@login' );
Route::post('getAuth' , 'Auth\AuthController@getAuth' );	
Route::post('register' , 'Auth\AuthController@register' );	
