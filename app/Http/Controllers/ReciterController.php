<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Zikr\Services\Validation\ReciterValidator;

use App\Reciter;

class ReciterController extends Controller {

	/**
	  	* @api {get} /reciter 1- Read all Playlist
   		* @apiGroup Reciter
 	 	* @apiName GetReciters
 		* @apiDescription no differences between versions of this api until now .
	 */
	public function index()
	{
		return Reciter::all();
	}

	/**

	 */
	public function create()
	{
		//
	}

	/**
		* @api {post} /reciter 3- Create a new Reciter
		* @apiParam {string} name require - alpha .
		* @apiParam {string} country require - alpha .
		* @apiParam {file} image require - maxSize|3mb .
		* @apiParamExample {json} Request-Example:
 		*     {
 		*       "name": "jhon", 
 		*       "country": "UK", 
 		*       "image": "mySmile.jpeg", 
 		*     }
		* @apiError ValidationError The fields of the new Reciter doesn't match our rules.
     	* @apiGroup Reciter
 	 	* @apiName PostReciter
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function store(Request $request , ReciterValidator $reciterValidator )
	{		
			$reciterValidator->validate($request->all());

			Reciter::create([
					'name' => $request->name,
					'country' => $request->country,
					'image' => $request->image,
				]
			);

			return response()->json(['Success' => 'reciter has created successfully'],200);
	}

	/**
		* @api {get} /reciter/:id 2- Show Reciter data
		* @apiParam {string} id Users-ID.
		* @apiError UserNotFound The <code>id</code> of the Reciter was not found.
     	* @apiGroup Reciter
 	 	* @apiName GetReciter
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function show(Reciter $reciter)
	{
		return $reciter;
	}

	/**

	 */
	public function edit($id)
	{
		//
	}

	/**
		* @api {put} /reciter/:id 4- Change Reciter data
		* @apiParam {string} name require - alpha .
		* @apiParam {string} country require - alpha .
		* @apiParam {file} image require - maxSize|3mb .
		* @apiParamExample {json} Request-Example:
 		*     {
 		*       "name": "jhon", 
 		*       "country": "UK", 
 		*       "image": "mySmile.jpeg", 
 		*     }
		* @apiError ModelNotFound The <code>id</code> of the Reciter was not found.
		* @apiError ValidationError The fields of the new Reciter doesn't match our rules.
     	* @apiGroup Reciter
 	 	* @apiName PutReciter
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function update(Request $request , Reciter $reciter , ReciterValidator $reciterValidator)
	{
		$reciterValidator->validate($request->all());

		$reciter->name = $request->name ;
		$reciter->country = $request->country ;
		$reciter->image = $request->image ;

		$reciter->save();

		return response()->json(['message' => 'the reciter has successfully updated'] , 200);
	}

	/**
		* @api {delete} /reciter/:id 5- Delete Reciter data
		* @apiError ModalNotFound The <code>id</code> of the Reciter was not found.
     	* @apiGroup Reciter
 	 	* @apiName DeleteReciter
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function destroy(Reciter $reciter)
	{
		$reciter->delete();

		return response()->json(['message' => 'the reciter has successfully deleted'] , 200);
	}

}
