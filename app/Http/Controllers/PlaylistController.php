<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Zikr\Services\Validation\PlaylistValidator;

use App\Playlist;

class PlaylistController extends Controller {

	/**
	  	* @api {get} /playlist 1- Read all Playlists
   		* @apiGroup Playlist
 	 	* @apiName GetPlaylists
 		* @apiDescription no differences between versions of this api until now .
	 */
	public function index(Request $request)
	{
		return Playlist::all();
	}

	/**

	 */
	public function create()
	{
		//
	}

	/**
		* @api {post} /playlist 3- Create a new Playlist
		* @apiParam {string} title require - alpha - unique .
		* @apiParamExample {json} Request-Example:
 		*     {
 		*       "title": "MixSoundtracks",  
 		*     }
		* @apiError ValidationError The fields of the new Playlist doesn't match our rules.
     	* @apiGroup Playlist
 	 	* @apiName PostPlaylist
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function store(Request $request , PlaylistValidator $PlaylistValidator )
	{		
			$PlaylistValidator->validate($request->all());

			Playlist::create([
					'title' => $request->title,
				]
			);

			return response()->json(['message' => 'Playlist has created successfully'],200);
	}

	/**
		* @api {get} /playlist/:id 2- Show Playlist data
		* @apiParam {string} id Users-ID.
		* @apiError UserNotFound The <code>id</code> of the Playlist was not found.
     	* @apiGroup Playlist
 	 	* @apiName GetPlaylist
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function show(Playlist $playlist)
	{
		return $playlist;
	}

	/**

	 */
	public function edit($id)
	{
		//
	}

	/**
		* @api {put} /playlist/:id 4- Change Playlist data
		* @apiParam {string} title require - alpha - unique .
		* @apiParamExample {json} Request-Example:
 		*     {
 		*       "title": "Primary SoundTracks",  
 		*     }
		* @apiError ModelNotFound The <code>id</code> of the Playlist was not found.
		* @apiError ValidationError The fields of the new Playlist doesn't match our rules.
     	* @apiGroup Playlist
 	 	* @apiName PutPlaylist
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function update(Request $request , Playlist $playlist , PlaylistValidator $playlistValidator)
	{
		$playlistValidator->validate($request->all());

		$playlist->title = $request->title ;

		$playlist->save();

		return response()->json(['message' => 'the playlist has successfully updated'] , 200);
	}

	/**
		* @api {delete} /playlist/:id 5- Delete Playlist data
		* @apiError ModalNotFound The <code>id</code> of the Playlist was not found.
     	* @apiGroup Playlist
 	 	* @apiName DeleteReciter
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function destroy(Playlist $playlist)
	{
		$playlist->delete();

		return response()->json(['message' => 'the playlist has successfully deleted'] , 200);
	}

}
