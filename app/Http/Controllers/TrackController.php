<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Zikr\Services\Validation\TrackValidator;

use App\Track;

class TrackController extends Controller {

	/**
	  	* @api {get} /track 1- Read all Tracks
   		* @apiGroup Track
 	 	* @apiName GetTracks
 		* @apiDescription no differences between versions of this api until now .
	 */
	public function index()
	{
		return Track::all();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
		* @api {post} /track 3- Create a new Track
		* @apiParam {string} title require - alpha .
		* @apiParam {string} url require - alpha .
		* @apiParamExample {json} Request-Example:
 		*     {
 		*       "title": "Al Maouzatain", 
 		*       "url": "http://server11.mp3quran.net/shatri/###.mp3", 
 		*     }
		* @apiError ValidationError The fields of the new Track doesn't match our rules.
     	* @apiGroup Track
 	 	* @apiName PostTrack
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function store(Request $request , TrackValidator $trackValidator )
	{		
			$trackValidator->validate($request->all());

			Track::create([
					'title' => $request->title,
					'url' => $request->url,
				]
			);

			return response()->json(['message' => 'track has created successfully'],200);
	}

	/**
		* @api {get} /track/:id 2- Show Track data
		* @apiParam {string} id Users-ID.
		* @apiError UserNotFound The <code>id</code> of the Track was not found.
     	* @apiGroup Track
 	 	* @apiName GetTrack
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function show(track $track)
	{
		return $track;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
		* @api {put} /track/:id 4- Change Track data
		* @apiParam {string} title require - alpha .
		* @apiParam {string} url require - alpha .
		* @apiParamExample {json} Request-Example:
 		*     {
 		*       "title": "Al Maouzatain", 
 		*       "url": "http://server11.islamweb.net/mashary/###.mp3", 
 		*     }
		* @apiError ModelNotFound The <code>id</code> of the Track was not found.
		* @apiError ValidationError The fields of the new Track doesn't match our rules.
     	* @apiGroup Track
 	 	* @apiName PutTrack
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function update(Request $request , Track $track , TrackValidator $trackValidator)
	{
		$trackValidator->validate($request->all());

		$track->title = $request->title ;
		$track->url = $request->url ;

		$track->save();

		return response()->json(['message' => 'the track has successfully updated'] , 200);
	}

	/**
		* @api {delete} /track/:id 5- Delete Track data
		* @apiError ModalNotFound The <code>id</code> of the Track was not found.
     	* @apiGroup Track
 	 	* @apiName eleteReciter
 	 	* @apiDescription no differences between versions of this api until now .
	 */
	public function destroy(Track $track)
	{
		$track->delete();

		return response()->json(['message' => 'the track has successfully deleted'] , 200);
	}

}
