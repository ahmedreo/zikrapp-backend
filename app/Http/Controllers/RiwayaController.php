<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Zikr\Services\Validation\RiwayaValidator;

use App\Riwaya;

class RiwayaController extends Controller {

    /**
        * @api {get} /riwaya 1- Read all Riwaya
        * @apiGroup Riwaya
        * @apiName GetRiwayas
        * @apiDescription no differences between versions of this api until now .
     */
    public function index()
    {
        return Riwaya::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
        * @api {post} /riwaya 3- Create a new Riwaya
        * @apiParam {string} title require - alpha .
        * @apiParamExample {json} Request-Example:
        *     {
        *       "title": "hafs", 
        *     }
        * @apiError ValidationError The fields of the new Riwaya doesn't match our rules.
        * @apiGroup Riwaya
        * @apiName PostRiwaya
        * @apiDescription no differences between versions of this api until now .
     */
    public function store(Request $request , RiwayaValidator $riwayaValidator )
    {       
            $riwayaValidator->validate($request->all());

            Riwaya::create([
                    'title' => $request->title,
                ]
            );

            return response()->json(['message' => 'riwaya has created successfully'],200);
    }

    /**
        * @api {get} /riwaya/:id 2- Show Riwaya data
        * @apiParam {string} id Users-ID.
        * @apiError UserNotFound The <code>id</code> of the Riwaya was not found.
        * @apiGroup Riwaya
        * @apiName GetRiwaya
        * @apiDescription no differences between versions of this api until now .
     */
    public function show(Riwaya $riwaya)
    {
        return $riwaya;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
        * @api {put} /riwaya/:id 4- Change Riwaya data
        * @apiParam {string} title require - alpha .
        * @apiParamExample {json} Request-Example:
        *     {
        *       "title": "hafs", 
        *     }
        * @apiError ModelNotFound The <code>id</code> of the Riwaya was not found.
        * @apiError ValidationError The fields of the new Riwaya doesn't match our rules.
        * @apiGroup Riwaya
        * @apiName PutRiwaya
        * @apiDescription no differences between versions of this api until now .
     */
    public function update(Request $request , Riwaya $riwaya , RiwayaValidator $riwayaValidator)
    {
        $riwayaValidator->validate($request->all());

        $riwaya->title = $request->title ;

        $riwaya->save();

        return response()->json(['message' => 'the riwaya has successfully updated'] , 200);
    }

    /**
        * @api {delete} /riwaya/:id 5- Delete Riwaya data
        * @apiError ModalNotFound The <code>id</code> of the Riwaya was not found.
        * @apiGroup Riwaya
        * @apiName eleteReciter
        * @apiDescription no differences between versions of this api until now .
     */
    public function destroy(Riwaya $riwaya)
    {
        $riwaya->delete();

        return response()->json(['message' => 'the riwaya has successfully deleted'] , 200);
    }

}
