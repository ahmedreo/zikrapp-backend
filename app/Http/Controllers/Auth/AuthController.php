<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Faker\Factory as Facker ;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;


class AuthController extends Controller
{
    protected function login(Request $request)
    {
        
       if (Auth::attempt(['email' => $request->email, 'password' => $request->password ])) 
       {
            $user = Auth::user();

            $user->token = Facker::create()->md5();

            $user->save();

            return response()->json(['token' => Auth::user()->token ], 200);
        } 

        else{

            return response()->json(['message' => 'sorry your credintials does not match our records' ], 400);
        }
    }

    protected function register(Request $request)
    {
        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'token' => null,
        ]);

        return response()->json(['resp' => 'account created successfully' , 'user' => $user], 200);
    }
    protected function getAuth (Request $request)
    {
        $auth = User::where('token' , $request->token)->first();

        return response()->json(['user' => $auth]);
    }
}
