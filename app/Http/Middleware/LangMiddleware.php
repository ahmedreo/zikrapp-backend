<?php

namespace App\Http\Middleware;

use Closure;

use App;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('Accept-Language')) {
            
            App::setLocale($request->header('Accept-Language'));


        }
        
        return $next($request);
    }
}
