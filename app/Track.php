<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Track extends Eloquent {

	protected $fillable = ['name'];

}
