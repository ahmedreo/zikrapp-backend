<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Reciter extends Eloquent {

	protected $collection = 'reciters';

	protected $connection = 'mongodb';

	protected $fillable = ['name','country','image'];

	public function tracks()
	{
		return $this->hasMany(Track::class);
	}
	

}
