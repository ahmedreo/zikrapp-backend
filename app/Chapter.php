<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Chapter extends Eloquent
{
    protected $fillable = ['title' ,'verses' , 'place'];

    protected $hidden = [
        'en'
    ];
}
