<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Playlist extends Eloquent {

	protected $collection = 'playlists';

	protected $connection = 'mongodb';

	protected $fillable = ['title' , 'name'];
	

}