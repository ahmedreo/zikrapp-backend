<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Riwaya extends Eloquent
{
    protected $fillable = ['name'];
}
