<?php

namespace App\Zikr\Services\Validation;

class PlaylistValidator extends ValidateOrFail {

	public $rules = ['title' => 'required|alpha|unique:playlists'];

}